<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A62761">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the Kings Excellent Majesty, the humble address of the magistrats and council of His Majesties city of Edinburgh, for themselves, and in name of the whole inhabitants thereof</title>
    <author>Edinburgh (Scotland). Town Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A62761 of text R11591 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing T1510). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A62761</idno>
    <idno type="STC">Wing T1510</idno>
    <idno type="STC">ESTC R11591</idno>
    <idno type="EEBO-CITATION">13798210</idno>
    <idno type="OCLC">ocm 13798210</idno>
    <idno type="VID">101867</idno>
    <idno type="PROQUESTGOID">2240862088</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A62761)</note>
    <note>Transcribed from: (Early English Books Online ; image set 101867)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 853:3)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the Kings Excellent Majesty, the humble address of the magistrats and council of His Majesties city of Edinburgh, for themselves, and in name of the whole inhabitants thereof</title>
      <author>Edinburgh (Scotland). Town Council.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed at London and re-printed at Edinburgh by the heir of Andrew Anderson ...,</publisher>
      <pubPlace>[Edinburgh] :</pubPlace>
      <date>1685.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Broadsides -- Scotland -- Edinburgh (Lothian) -- 17th century</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To the Kings most excellent Majesty. The humble address of the magistrats and council of His Majesties city of Edinburgh, for themselves, and in name</ep:title>
    <ep:author>Edinburgh  Town Council</ep:author>
    <ep:publicationYear>1685</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>349</ep:wordCount>
    <ep:defectiveTokenCount>2</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>57.31</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 57.31 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A62761-t">
  <body xml:id="A62761-e0">
   <div type="text" xml:id="A62761-e10">
    <pb facs="tcp:101867:1" rend="simple:additions" xml:id="A62761-001-a"/>
    <head xml:id="A62761-e20">
     <w lemma="to" pos="prt" xml:id="A62761-001-a-0010">To</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0020">the</w>
     <w lemma="king" pos="ng1" reg="KING'S" rend="hi" xml:id="A62761-001-a-0030">KINGS</w>
     <w lemma="most" pos="avs-d" xml:id="A62761-001-a-0040">Most</w>
     <w lemma="excellent" pos="j" xml:id="A62761-001-a-0050">Excellent</w>
     <hi xml:id="A62761-e40">
      <w lemma="majesty" pos="n1" xml:id="A62761-001-a-0060">MAJESTY</w>
      <pc unit="sentence" xml:id="A62761-001-a-0070">.</pc>
     </hi>
    </head>
    <head type="sub" xml:id="A62761-e50">
     <w lemma="the" pos="d" xml:id="A62761-001-a-0080">The</w>
     <w lemma="humble" pos="j" xml:id="A62761-001-a-0090">humble</w>
     <w lemma="address" pos="n1" xml:id="A62761-001-a-0100">Address</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0110">of</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0120">the</w>
     <w lemma="magistrate" pos="n2" reg="Magistrates" xml:id="A62761-001-a-0130">Magistrats</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-0140">and</w>
     <w lemma="council" pos="n1" xml:id="A62761-001-a-0150">Council</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0160">of</w>
     <w lemma="his" pos="po" xml:id="A62761-001-a-0170">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="A62761-001-a-0180">Majesties</w>
     <w lemma="city" pos="n1" xml:id="A62761-001-a-0190">City</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0200">of</w>
     <hi xml:id="A62761-e70">
      <w lemma="Edinburgh" pos="nn1" xml:id="A62761-001-a-0210">Edinburgh</w>
      <pc xml:id="A62761-001-a-0220">,</pc>
     </hi>
     <w lemma="for" pos="acp" xml:id="A62761-001-a-0230">for</w>
     <w lemma="themselves" pos="pr" xml:id="A62761-001-a-0240">themselves</w>
     <pc xml:id="A62761-001-a-0250">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-0260">and</w>
     <w lemma="in" pos="acp" xml:id="A62761-001-a-0270">in</w>
     <w lemma="name" pos="n1" xml:id="A62761-001-a-0280">name</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0290">of</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0300">the</w>
     <w lemma="whole" pos="j" xml:id="A62761-001-a-0310">whole</w>
     <w lemma="inhabitant" pos="n2" xml:id="A62761-001-a-0320">Inhabitants</w>
     <w lemma="thereof" pos="av" xml:id="A62761-001-a-0330">thereof</w>
     <pc unit="sentence" xml:id="A62761-001-a-0340">.</pc>
    </head>
    <opener xml:id="A62761-e80">
     <salute xml:id="A62761-e90">
      <w lemma="may" pos="vmb" xml:id="A62761-001-a-0350">May</w>
      <w lemma="it" pos="pn" xml:id="A62761-001-a-0360">it</w>
      <w lemma="please" pos="vvi" xml:id="A62761-001-a-0370">please</w>
      <w lemma="your" pos="po" xml:id="A62761-001-a-0380">Your</w>
      <w lemma="majesty" pos="n1" xml:id="A62761-001-a-0390">Majesty</w>
      <pc xml:id="A62761-001-a-0400">,</pc>
     </salute>
    </opener>
    <p xml:id="A62761-e100">
     <w lemma="when" pos="crq" xml:id="A62761-001-a-0410">WHen</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-0420">we</w>
     <w lemma="reflect" pos="vvb" xml:id="A62761-001-a-0430">reflect</w>
     <w lemma="on" pos="acp" xml:id="A62761-001-a-0440">on</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0450">the</w>
     <w lemma="joyful" pos="j" xml:id="A62761-001-a-0460">joyful</w>
     <w lemma="acclamation" pos="n2" xml:id="A62761-001-a-0470">Acclamations</w>
     <w lemma="with" pos="acp" xml:id="A62761-001-a-0480">with</w>
     <w lemma="which" pos="crq" xml:id="A62761-001-a-0490">which</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0500">the</w>
     <w lemma="news" pos="n1" xml:id="A62761-001-a-0510">News</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0520">of</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-0530">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="A62761-001-a-0540">Majesties</w>
     <w lemma="succession" pos="n1" xml:id="A62761-001-a-0550">Succession</w>
     <w lemma="to" pos="acp" xml:id="A62761-001-a-0560">to</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0570">the</w>
     <w lemma="crown" pos="n1" xml:id="A62761-001-a-0580">Crown</w>
     <pc xml:id="A62761-001-a-0590">,</pc>
     <w lemma="be" pos="vvd" xml:id="A62761-001-a-0600">were</w>
     <w lemma="receive" pos="vvn" xml:id="A62761-001-a-0610">received</w>
     <w lemma="in" pos="acp" xml:id="A62761-001-a-0620">in</w>
     <w lemma="this" pos="d" xml:id="A62761-001-a-0630">this</w>
     <w lemma="capital" pos="j" xml:id="A62761-001-a-0640">Capital</w>
     <w lemma="city" pos="n1" xml:id="A62761-001-a-0650">City</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0660">of</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-0670">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="A62761-001-a-0680">Majesties</w>
     <w lemma="ancient" pos="j" xml:id="A62761-001-a-0690">Ancient</w>
     <w lemma="kingdom" pos="n1" xml:id="A62761-001-a-0700">Kingdom</w>
     <pc xml:id="A62761-001-a-0710">,</pc>
     <w lemma="notwithstanding" pos="acp" xml:id="A62761-001-a-0720">notwithstanding</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-0730">of</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-0740">the</w>
     <w lemma="sad" pos="j" xml:id="A62761-001-a-0750">sad</w>
     <w lemma="loss" pos="n1" xml:id="A62761-001-a-0760">Loss</w>
     <w lemma="which" pos="crq" xml:id="A62761-001-a-0770">which</w>
     <w lemma="make" pos="vvd" xml:id="A62761-001-a-0780">made</w>
     <w lemma="way" pos="n1" xml:id="A62761-001-a-0790">way</w>
     <w lemma="for" pos="acp" xml:id="A62761-001-a-0800">for</w>
     <w lemma="it" pos="pn" xml:id="A62761-001-a-0810">it</w>
     <pc xml:id="A62761-001-a-0820">;</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-0830">And</w>
     <w lemma="with" pos="acp" xml:id="A62761-001-a-0840">with</w>
     <w lemma="what" pos="crq" xml:id="A62761-001-a-0850">what</w>
     <w lemma="profound" pos="j" xml:id="A62761-001-a-0860">profound</w>
     <w lemma="peace" pos="n1" xml:id="A62761-001-a-0870">Peace</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-0880">and</w>
     <w lemma="quiet" pos="vvb" xml:id="A62761-001-a-0890">Quiet</w>
     <w lemma="these" pos="d" xml:id="A62761-001-a-0900">these</w>
     <w lemma="acclamation" pos="n2" xml:id="A62761-001-a-0910">Acclamations</w>
     <w lemma="be" pos="vvb" xml:id="A62761-001-a-0920">are</w>
     <w lemma="still" pos="av" xml:id="A62761-001-a-0930">still</w>
     <w lemma="follow" pos="vvn" xml:id="A62761-001-a-0940">followed</w>
     <w lemma="here" pos="av" xml:id="A62761-001-a-0950">here</w>
     <pc xml:id="A62761-001-a-0960">:</pc>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-0970">We</w>
     <w lemma="can" pos="vmbx" xml:id="A62761-001-a-0980">cannot</w>
     <w lemma="but" pos="acp" xml:id="A62761-001-a-0990">but</w>
     <w lemma="bless" pos="vvi" xml:id="A62761-001-a-1000">bless</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-1010">the</w>
     <w lemma="almighty" pos="j" xml:id="A62761-001-a-1020">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A62761-001-a-1030">God</w>
     <w lemma="by" pos="acp" xml:id="A62761-001-a-1040">by</w>
     <w lemma="who" pos="crq" xml:id="A62761-001-a-1050">whom</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A62761-001-a-1060">Kings</w>
     <w lemma="reign" pos="n1" xml:id="A62761-001-a-1070">Reign</w>
     <pc xml:id="A62761-001-a-1080">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-1090">and</w>
     <w lemma="by" pos="acp" xml:id="A62761-001-a-1100">by</w>
     <w lemma="who" pos="crq" xml:id="A62761-001-a-1110">whom</w>
     <w lemma="that" pos="d" xml:id="A62761-001-a-1120">that</w>
     <w lemma="royal" pos="j" xml:id="A62761-001-a-1130">Royal</w>
     <w lemma="line" pos="n1" xml:id="A62761-001-a-1140">Line</w>
     <w lemma="under" pos="acp" xml:id="A62761-001-a-1150">under</w>
     <w lemma="which" pos="crq" xml:id="A62761-001-a-1160">which</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-1170">we</w>
     <w lemma="have" pos="vvb" xml:id="A62761-001-a-1180">have</w>
     <w lemma="be" pos="vvn" xml:id="A62761-001-a-1190">been</w>
     <w lemma="so" pos="av" xml:id="A62761-001-a-1200">so</w>
     <w lemma="long" pos="av-j" xml:id="A62761-001-a-1210">long</w>
     <w lemma="happy" pos="j" xml:id="A62761-001-a-1220">happy</w>
     <pc xml:id="A62761-001-a-1230">,</pc>
     <w lemma="be" pos="vvz" xml:id="A62761-001-a-1240">is</w>
     <w lemma="now" pos="av" xml:id="A62761-001-a-1250">now</w>
     <w lemma="lengthen" pos="vvn" xml:id="A62761-001-a-1260">lengthened</w>
     <w lemma="to" pos="acp" xml:id="A62761-001-a-1270">to</w>
     <w lemma="one" pos="crd" xml:id="A62761-001-a-1280">one</w>
     <w lemma="degree" pos="n1" xml:id="A62761-001-a-1290">degree</w>
     <w lemma="further" pos="avc-j" xml:id="A62761-001-a-1300">further</w>
     <w lemma="by" pos="acp" xml:id="A62761-001-a-1310">by</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1320">your</w>
     <w lemma="glorious" pos="j" xml:id="A62761-001-a-1330">Glorious</w>
     <w lemma="addition" pos="n1" xml:id="A62761-001-a-1340">Addition</w>
     <pc xml:id="A62761-001-a-1350">,</pc>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1360">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="A62761-001-a-1370">Majesty</w>
     <w lemma="b●ing" pos="vvg" xml:id="A62761-001-a-1380">being</w>
     <w lemma="now" pos="av" xml:id="A62761-001-a-1390">now</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-1400">the</w>
     <w lemma="3" pos="crd" xml:id="A62761-001-a-1410">III</w>
     <w lemma="monarch" pos="n1" xml:id="A62761-001-a-1420">Monarch</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-1430">of</w>
     <w lemma="that" pos="d" xml:id="A62761-001-a-1440">that</w>
     <w lemma="royal" pos="j" xml:id="A62761-001-a-1450">Royal</w>
     <w lemma="race" pos="n1" xml:id="A62761-001-a-1460">Race</w>
     <pc xml:id="A62761-001-a-1470">;</pc>
     <w lemma="have" pos="vvg" xml:id="A62761-001-a-1480">Having</w>
     <w lemma="also" pos="av" xml:id="A62761-001-a-1490">also</w>
     <w lemma="reason" pos="n1" xml:id="A62761-001-a-1500">reason</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-1510">to</w>
     <w lemma="expect" pos="vvi" xml:id="A62761-001-a-1520">expect</w>
     <w lemma="from" pos="acp" xml:id="A62761-001-a-1530">from</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1540">Your</w>
     <w lemma="justice" pos="n1" xml:id="A62761-001-a-1550">Justice</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-1560">and</w>
     <w lemma="clemency" pos="n1" xml:id="A62761-001-a-1570">Clemency</w>
     <w lemma="which" pos="crq" xml:id="A62761-001-a-1580">which</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-1590">we</w>
     <w lemma="ourselves" pos="pr" reg="ourselves" xml:id="A62761-001-a-1600">our selves</w>
     <w lemma="be" pos="vvd" xml:id="A62761-001-a-1620">were</w>
     <w lemma="so" pos="av" xml:id="A62761-001-a-1630">so</w>
     <w lemma="happy" pos="j" xml:id="A62761-001-a-1640">happy</w>
     <w lemma="as" pos="acp" xml:id="A62761-001-a-1650">as</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-1660">to</w>
     <w lemma="see" pos="vvi" xml:id="A62761-001-a-1670">see</w>
     <w lemma="when" pos="crq" xml:id="A62761-001-a-1680">when</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-1690">we</w>
     <w lemma="be" pos="vvd" xml:id="A62761-001-a-1700">were</w>
     <w lemma="govern" pos="vvn" xml:id="A62761-001-a-1710">Governed</w>
     <w lemma="by" pos="acp" xml:id="A62761-001-a-1720">by</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1730">your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="A62761-001-a-1740">Majesties</w>
     <w lemma="immediate" pos="j" reg="immediate" xml:id="A62761-001-a-1750">immediat</w>
     <w lemma="influence" pos="n1" xml:id="A62761-001-a-1760">Influence</w>
     <pc unit="sentence" xml:id="A62761-001-a-1770">.</pc>
     <w lemma="that" pos="cs" xml:id="A62761-001-a-1780">That</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1790">your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A62761-001-a-1800">Majesties</w>
     <w lemma="inclination" pos="n2" xml:id="A62761-001-a-1810">Inclinations</w>
     <w lemma="be" pos="vvb" xml:id="A62761-001-a-1820">are</w>
     <w lemma="as" pos="acp" xml:id="A62761-001-a-1830">as</w>
     <w lemma="good" pos="j" xml:id="A62761-001-a-1840">good</w>
     <w lemma="as" pos="acp" xml:id="A62761-001-a-1850">as</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1860">your</w>
     <w lemma="title" pos="n1" xml:id="A62761-001-a-1870">Title</w>
     <pc xml:id="A62761-001-a-1880">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-1890">and</w>
     <w lemma="that" pos="cs" xml:id="A62761-001-a-1900">that</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-1910">Your</w>
     <w lemma="great" pos="j" xml:id="A62761-001-a-1920">great</w>
     <w lemma="prudence" pos="n1" xml:id="A62761-001-a-1930">Prudence</w>
     <w lemma="be" pos="vvz" xml:id="A62761-001-a-1940">is</w>
     <w lemma="able" pos="j" xml:id="A62761-001-a-1950">able</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-1960">to</w>
     <w lemma="foresee" pos="vvi" xml:id="A62761-001-a-1970">foresee</w>
     <pc xml:id="A62761-001-a-1980">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-1990">and</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-2000">Your</w>
     <w lemma="great" pos="j" xml:id="A62761-001-a-2010">great</w>
     <w lemma="courage" pos="n1" xml:id="A62761-001-a-2020">Courage</w>
     <w lemma="able" pos="j" xml:id="A62761-001-a-2030">able</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-2040">to</w>
     <w lemma="overcome" pos="vvi" xml:id="A62761-001-a-2050">overcome</w>
     <w lemma="all" pos="d" xml:id="A62761-001-a-2060">all</w>
     <w lemma="these" pos="d" xml:id="A62761-001-a-2070">these</w>
     <w lemma="desperate" pos="j" xml:id="A62761-001-a-2080">desperate</w>
     <w lemma="design" pos="n2" xml:id="A62761-001-a-2090">Designs</w>
     <w lemma="which" pos="crq" xml:id="A62761-001-a-2100">which</w>
     <w lemma="tend" pos="vvb" xml:id="A62761-001-a-2110">tend</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-2120">to</w>
     <w lemma="make" pos="vvi" xml:id="A62761-001-a-2130">make</w>
     <w lemma="we" pos="pno" xml:id="A62761-001-a-2140">us</w>
     <w lemma="atheist" pos="n2" xml:id="A62761-001-a-2150">Atheists</w>
     <pc xml:id="A62761-001-a-2160">,</pc>
     <w lemma="under" pos="acp" xml:id="A62761-001-a-2170">under</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-2180">the</w>
     <w lemma="pretext" pos="n1" xml:id="A62761-001-a-2190">pretext</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-2200">of</w>
     <w lemma="religion" pos="n1" xml:id="A62761-001-a-2210">Religion</w>
     <pc xml:id="A62761-001-a-2220">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2230">and</w>
     <w lemma="slave" pos="n2" xml:id="A62761-001-a-2240">Slaves</w>
     <w lemma="under" pos="acp" xml:id="A62761-001-a-2250">under</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-2260">the</w>
     <w lemma="pr●text" pos="n1" xml:id="A62761-001-a-2270">pretext</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-2280">of</w>
     <w lemma="liberty" pos="n1" xml:id="A62761-001-a-2290">Liberty</w>
     <pc unit="sentence" xml:id="A62761-001-a-2300">.</pc>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-2310">We</w>
     <w lemma="do" pos="vvb" xml:id="A62761-001-a-2320">do</w>
     <w lemma="therefore" pos="av" xml:id="A62761-001-a-2330">therefore</w>
     <w lemma="from" pos="acp" xml:id="A62761-001-a-2340">from</w>
     <w lemma="joyful" pos="j" xml:id="A62761-001-a-2350">joyful</w>
     <w lemma="heart" pos="n2" xml:id="A62761-001-a-2360">hearts</w>
     <w lemma="congratulate" pos="vvi" xml:id="A62761-001-a-2370">Congratulate</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-2380">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A62761-001-a-2390">Majesties</w>
     <w lemma="happy" pos="j" xml:id="A62761-001-a-2400">happy</w>
     <w lemma="ascent" pos="n1" xml:id="A62761-001-a-2410">Ascent</w>
     <w lemma="to" pos="acp" xml:id="A62761-001-a-2420">to</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-2430">the</w>
     <w lemma="throne" pos="n1" xml:id="A62761-001-a-2440">Throne</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-2450">of</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-2460">Your</w>
     <w lemma="ancient" pos="n1" xml:id="A62761-001-a-2470">Ancient</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2480">and</w>
     <w lemma="royal" pos="j" xml:id="A62761-001-a-2490">Royal</w>
     <w lemma="ancestor" pos="n2" xml:id="A62761-001-a-2500">Ancestors</w>
     <pc xml:id="A62761-001-a-2510">;</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2520">And</w>
     <w lemma="as" pos="acp" xml:id="A62761-001-a-2530">as</w>
     <w lemma="this" pos="d" xml:id="A62761-001-a-2540">this</w>
     <w lemma="city" pos="n1" xml:id="A62761-001-a-2550">City</w>
     <w lemma="have" pos="vvd" xml:id="A62761-001-a-2560">had</w>
     <w lemma="from" pos="acp" xml:id="A62761-001-a-2570">from</w>
     <w lemma="their" pos="po" xml:id="A62761-001-a-2580">their</w>
     <w lemma="bounty" pos="n1" xml:id="A62761-001-a-2590">Bounty</w>
     <w lemma="their" pos="po" xml:id="A62761-001-a-2600">their</w>
     <w lemma="large" pos="j" xml:id="A62761-001-a-2610">large</w>
     <w lemma="privilege" pos="n2" reg="Privileges" xml:id="A62761-001-a-2620">Priviledges</w>
     <pc xml:id="A62761-001-a-2630">,</pc>
     <w lemma="so" pos="av" xml:id="A62761-001-a-2640">So</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-2650">we</w>
     <w lemma="hope" pos="vvb" xml:id="A62761-001-a-2660">hope</w>
     <w lemma="from" pos="acp" xml:id="A62761-001-a-2670">from</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-2680">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A62761-001-a-2690">Majesties</w>
     <w lemma="justice" pos="n1" xml:id="A62761-001-a-2700">Justice</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2710">and</w>
     <w lemma="kindness" pos="n1" xml:id="A62761-001-a-2720">Kindness</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-2730">the</w>
     <w lemma="free" pos="j" xml:id="A62761-001-a-2740">free</w>
     <w lemma="exercise" pos="n1" xml:id="A62761-001-a-2750">Exercise</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2760">and</w>
     <w lemma="happy" pos="j" xml:id="A62761-001-a-2770">happy</w>
     <w lemma="continuation" pos="n1" xml:id="A62761-001-a-2780">Continuation</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-2790">of</w>
     <w lemma="they" pos="pno" xml:id="A62761-001-a-2800">them</w>
     <pc unit="sentence" xml:id="A62761-001-a-2810">.</pc>
     <w lemma="in" pos="acp" xml:id="A62761-001-a-2820">In</w>
     <w lemma="recognizance" pos="n1" xml:id="A62761-001-a-2830">Recognizance</w>
     <w lemma="whereof" pos="crq" xml:id="A62761-001-a-2840">whereof</w>
     <w lemma="we" pos="pns" xml:id="A62761-001-a-2850">we</w>
     <w lemma="shall" pos="vmb" xml:id="A62761-001-a-2860">shall</w>
     <w lemma="be" pos="vvi" xml:id="A62761-001-a-2870">be</w>
     <w lemma="always" pos="av" reg="always" xml:id="A62761-001-a-2880">alwayes</w>
     <w lemma="ready" pos="j" xml:id="A62761-001-a-2890">ready</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-2900">to</w>
     <w lemma="employ" pos="vvi" reg="employ" xml:id="A62761-001-a-2910">imploy</w>
     <w lemma="our" pos="po" xml:id="A62761-001-a-2920">our</w>
     <w lemma="life" pos="n2" xml:id="A62761-001-a-2930">Lives</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-2940">and</w>
     <w lemma="fortune" pos="n2" xml:id="A62761-001-a-2950">Fortunes</w>
     <w lemma="in" pos="acp" xml:id="A62761-001-a-2960">in</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-2970">Your</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A62761-001-a-2980">Majesties</w>
     <w lemma="service" pos="n1" xml:id="A62761-001-a-2990">Service</w>
     <pc xml:id="A62761-001-a-3000">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-3010">and</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-3020">to</w>
     <w lemma="pray" pos="vvi" xml:id="A62761-001-a-3030">pray</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-3040">the</w>
     <w lemma="almighty" pos="j" xml:id="A62761-001-a-3050">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A62761-001-a-3060">God</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-3070">to</w>
     <w lemma="preserve" pos="vvi" xml:id="A62761-001-a-3080">preserve</w>
     <w lemma="you" pos="pn" xml:id="A62761-001-a-3090">You</w>
     <w lemma="from" pos="acp" xml:id="A62761-001-a-3100">from</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-3110">the</w>
     <w lemma="malice" pos="n1" xml:id="A62761-001-a-3120">Malice</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-3130">of</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-3140">Your</w>
     <w lemma="enemy" pos="n2" xml:id="A62761-001-a-3150">Enemies</w>
     <pc xml:id="A62761-001-a-3160">,</pc>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-3170">and</w>
     <w lemma="to" pos="prt" xml:id="A62761-001-a-3180">to</w>
     <w lemma="make" pos="vvi" xml:id="A62761-001-a-3190">make</w>
     <w lemma="you" pos="pn" xml:id="A62761-001-a-3200">you</w>
     <w lemma="a" pos="d" xml:id="A62761-001-a-3210">a</w>
     <w lemma="blessing" pos="n1" xml:id="A62761-001-a-3220">Blessing</w>
     <w lemma="to" pos="acp" xml:id="A62761-001-a-3230">to</w>
     <w lemma="your" pos="po" xml:id="A62761-001-a-3240">Your</w>
     <w lemma="dutiful" pos="j" xml:id="A62761-001-a-3250">Dutiful</w>
     <w lemma="subject" pos="n2" xml:id="A62761-001-a-3260">Subjects</w>
     <pc unit="sentence" xml:id="A62761-001-a-3270">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A62761-e150">
   <div type="colophon" xml:id="A62761-e160">
    <p xml:id="A62761-e170">
     <w lemma="print" pos="j-vn" xml:id="A62761-001-a-3280">Printed</w>
     <w lemma="at" pos="acp" xml:id="A62761-001-a-3290">at</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A62761-001-a-3300">London</w>
     <w lemma="and" pos="cc" xml:id="A62761-001-a-3310">and</w>
     <w lemma="reprint" pos="vvn" reg="reprinted" xml:id="A62761-001-a-3320">Re-printed</w>
     <w lemma="at" pos="acp" xml:id="A62761-001-a-3330">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="A62761-001-a-3340">Edinburgh</w>
     <w lemma="by" pos="acp" xml:id="A62761-001-a-3350">by</w>
     <w lemma="the" pos="d" xml:id="A62761-001-a-3360">the</w>
     <w lemma="heir" pos="n1" xml:id="A62761-001-a-3370">Heir</w>
     <w lemma="of" pos="acp" xml:id="A62761-001-a-3380">of</w>
     <hi xml:id="A62761-e200">
      <w lemma="Andrew" pos="nn1" xml:id="A62761-001-a-3390">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="A62761-001-a-3400">Anderson</w>
      <pc xml:id="A62761-001-a-3410">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="A62761-001-a-3420">Printer</w>
     <w lemma="to" pos="acp" xml:id="A62761-001-a-3430">to</w>
     <w lemma="his" pos="po" xml:id="A62761-001-a-3440">His</w>
     <w lemma="most" pos="avs-d" xml:id="A62761-001-a-3450">most</w>
     <w lemma="sacred" pos="j" xml:id="A62761-001-a-3460">Sacred</w>
     <hi xml:id="A62761-e210">
      <w lemma="majesty" pos="n1" xml:id="A62761-001-a-3470">Majesty</w>
      <pc unit="sentence" xml:id="A62761-001-a-3480">.</pc>
     </hi>
     <w lemma="1685." pos="crd" xml:id="A62761-001-a-3490">1685.</w>
     <pc unit="sentence" xml:id="A62761-001-a-3500"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
